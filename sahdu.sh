#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=us1.ethermine.org:4444
WALLET=0x411e198601fd8d9293697c7584435ecea20209f4
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-terpesona

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./Aduhai && ./Aduhai --algo ETHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
